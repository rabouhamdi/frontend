package com.example.bfu_helper_10;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import kotlin.jvm.internal.Intrinsics;

public class MainActivity extends AppCompatActivity {

    private static MainActivity instance;
    List<DayOfSchedule> days = new ArrayList<>();

    // static backdrop
    public static BottomSheetBehavior mBottomSheetBehavior;
    public static WindowFragment fragment;
    public static FragmentManager fragmentManager;

    public static MainActivity getInstance() {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);

        RelativeLayout main_layout = (RelativeLayout) findViewById(R.id.main_layout);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list);

        fragment = WindowFragment.newInstance();
        fragmentManager = getSupportFragmentManager();

        HttpController.getDaysOfTheWeek("https://grph.ru/bfu_app/get_schedule","{\n" +
                "    \"userId\": \"604264122\",\n" +
                "    \"sessionCode\": \"ed410412dd216bed2daab53eded2a04f\"\n" +
                "}", this, recyclerView,  (ArrayList<DayOfSchedule>) days);
        DataAdapter adapter = new DataAdapter(this, days);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.page_1:
                        main_layout.setBackgroundColor(Color.parseColor("#E0BDFB"));
                        recyclerView.setAdapter(null);
                        break;
                    case R.id.page_2:
                        main_layout.setBackgroundColor(Color.parseColor("#BDC0FB"));
                        recyclerView.setAdapter(null);
                        break;
                    case R.id.page_3:
                        main_layout.setBackgroundColor(Color.parseColor("#FFFFFF"));
                        recyclerView.setAdapter(adapter);

                        break;
                    default:
                        break;
                }
                return true;
            }
        });
    }



}